-- v1.0
-- BBDD Gestor repostaje

CREATE TABLE Refueling(
    id INT AUTO_INCREMENT NOT NULL,
	userId INT,
    currentKm FLOAT(4) NOT NULL,
	litersRefueled FLOAT (4) NOT NULL,
	pricePaid FLOAT (4) NOT NULL,
    dateRefueled DATETIME,
    carId INT NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (userId) REFERENCES Users(id),
    FOREIGN KEY (carId) REFERENCES Vehicles(id)
)ENGINE = INNODB;

CREATE TABLE Users (
	id INT AUTO_INCREMENT NOT NULL,
	userNick VARCHAR (20) NOT NULL,
	name VARCHAR(20) NOT NULL,
	email VARCHAR (50) NOT NULL,
	password VARCHAR(512),
	PRIMARY KEY (id)
)ENGINE = INNODB;

CREATE TABLE Vehicles (
	id INT AUTO_INCREMENT NOT NULL,
    idUser INT,
	carBrand VARCHAR (50) NOT NULL,
    PRIMARY KEY (id),
	FOREIGN KEY (idUser) REFERENCES Users(id)
)ENGINE = INNODB;
