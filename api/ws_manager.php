<?php
header('Content-Type: application/json');
header("Acces-Control-Allow-Origin: *");
header("Acces-Control-Allow-Methods: POST, GET, OPTIONS");

//--------------------------------------------------------------
// CONNET DB
//--------------------------------------------------------------

function connectDatabase(){
    $hostname="localhost";
    $username="paufmqga";
    $password="LB0OydrA";
    $dataBase="paufmqga_gestorRepostaje";

    $conexion = mysqli_connect($hostname,$username,$password,$dataBase) or die("No Conection". mysqli_connect_error());
    mysqli_set_charset($conexion,'utf8');
    return $conexion;
}

function closeDatabase($con){
    mysqli_close($con);
}

//--------------------------------------------------------------
// WEBSERVICE FUNCTIONS
//--------------------------------------------------------------

function getUser(){
    //Connect BBDD
    $conexion = connectDatabase();

    //Get JSON content
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON);
    $userNick = $input->userNick;
    $userPassword = $input->userPassword;

    //SQL
    $sql = "SELECT id,name,email FROM Users WHERE userNick ='$userNick' AND password='$userPassword'";
    $query = mysqli_query($conexion,$sql) or die(mysqli_error($conexion));
    $result = mysqli_fetch_array($query,MYSQLI_ASSOC);
    if($result>0){
        $response = array(  'success' => true,
        'id'    => $result['id'],
        'name'  => $result['name'],
        'email' => $result['email']);
    }else {
        $response = array(  'success' => false);
    }

    closeDatabase($conexion);

    return $response;
}
function setFuel(){

try {
    $conexion = connectDatabase();

    //Get JSON content
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON);

    //SQL
    $sth = $db_con->prepare('INSERT INTO Refueling (userId,currentKm,litersRefueled,pricePaid,priceLiter,DateRefueled)
    VALUES (:userId,:currentKm,:litersRefueled,:pricePaid,priceLiter,now());');

    $costLiter = ($input->pricePaid/$input->litersRefueled);

    $sth->bindParam(':userId', $input->userId);
    $sth->bindParam(':carId', $input->carId);
    $sth->bindParam(':currentKm', $input->currentKm);
    $sth->bindParam(':pricePaid', $input->pricePaid);
    $sth->bindParam(':priceLiter', $costLiter);
    $sth->bindParam(':litersRefueled', $input->litersRefueled);
    $sth->execute();

    $response = array(  'success' => true);

    closeDatabase($conexion);

    return $response;

} catch (Exception $e) {
    $response = array(  'success' => false,
    'reason'    => $result[2]);
}


}

//--------------------------------------------------------------
// CATCHER
//--------------------------------------------------------------

// Get URL data and process it to select the according function
switch ($_GET['module']) {
    case "fuel":
        switch ($_GET['action']) {
            case "getfuel":
                $resultados = getFuel();
            break;
            case "setFuel":
                $resultados = setFuel();
            break;
        }
        break;
    case "user":
        switch ($_GET['action']) {
            case "getUser":
                $resultados = getUser();
            break;
        }
        break;
    case "car":
        switch ($_GET['action']) {
            case "getCar":
                $resultados = getCar();
            break;
            case "setCar":
                $resultados = setCar();
            break;
        }
        break;
    default:
        header('HTTP/1.1 405 Method Not Allowed');
        $resultados = array(  'success' => false);
}


//Return JSON
echo json_encode($resultados);
?>
